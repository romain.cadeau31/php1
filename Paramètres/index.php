<?php

// ex1 index.php?nom=Nemare&prenom=Jean
// ex2 _ index.php?nom=Nemare&prenom=Jean_

$_GET ['nom'];
$_GET ['prenom'];
echo $_GET ['prenom'];
echo $_GET ['nom'];
echo "\n";
if(!isset($_GET ['age'])) {
    echo 'le querystring ne contient pas age';
    } else {
        echo $_GET ['age'];
    }    

?>

<?php

// ex3 index.php?dateDebut=2/05/2016&dateFin=27/11/2016

$_GET ['dateDebut'];
$_GET ['dateFin'];
echo $_GET ['dateDebut'];
echo "\n";
echo $_GET ['dateFin'];

?>

<?php

// ex4 index.php?langage=PHP&serveur=LAMP

$_GET ['langage'];
$_GET ['serveur'];
echo $_GET ['langage'];
echo "\n";
echo $_GET ['serveur'];

?>

<?php

// ex5 index.php?semaine=12

$_GET ['semaine'];
echo $_GET ['semaine'];

?>

<?php

// ex6 index.php?batiment=12&salle=101

$_GET ['batiment'];
$_GET ['salle'];
echo $_GET ['batiment'];
echo "\n";
echo $_GET ['salle'];

?>