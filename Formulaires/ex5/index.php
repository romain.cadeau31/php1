<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Formulaire</title>
    </head>

    <body>

    <form action="index.php" method="post">
        <select name="genre">
        <option value="Mr">Mr</option>
        <option value="Mme">Mme</option>
        </select>
        <p><label>Nom : <input type="text" name="nom" /></label></p>
        <p><label>Prénom : <input type="text" name="prenom" /></label></p>
        <p><input type="submit" /></p>
    </form>

    
    <p>
        Bonjour <?php echo htmlspecialchars($_POST['genre'].' '.$_POST['nom'].' '.$_POST['prenom']); ?>
    </p>

    </body>
</html>