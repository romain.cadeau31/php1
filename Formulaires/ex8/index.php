<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Formulaire</title>
    </head>

    <body>
<?php

    if (isset($_POST['nom']) AND isset($_POST['prenom']) AND (isset($_FILES['monfichier']) AND $_FILES['monfichier']['error'] == 0)) {
      
                if ($_FILES['monfichier']['size'] <= 1000000)
                {
                        $infosfichier = pathinfo($_FILES['monfichier']['name']);
                        $extension_upload = $infosfichier['extension'];
                        $extensions_autorisees = array('pdf');
                        if (in_array($extension_upload, $extensions_autorisees))
                        {
                                move_uploaded_file($_FILES['monfichier']['tmp_name'], 'uploads/' . basename($_FILES['monfichier']['name']));
                                echo "L'envoi a bien été effectué !\n";
                                echo "Bonjour ";
                                echo htmlspecialchars($_POST['genre'].' '.$_POST['nom'].' '.$_POST['prenom']);
                                echo "\n";
                                echo $_FILES['monfichier']['name'];
                                echo "\n";
                                echo $_FILES['monfichier']['type'];
                        }
                }
        



    } else { ?>
        <form action="index.php" method="post" enctype="multipart/form-data">
        <select name="genre">
        <option value="Mr">Mr</option>
        <option value="Mme">Mme</option>
        </select>
        <p><label>Nom : <input type="text" name="nom" /></label></p>
        <p><label>Prénom : <input type="text" name="prenom" /></label></p>
        <p>
                Formulaire d'envoi de fichier :<br />
                <input type="file" name="monfichier" accept="application/pdf"/><br />
                <input type="submit" value="Envoyer" />
        </p>
    </form> <?php
    }

?>

    </body>
</html>