<?php

session_start();

$_SESSION['nom'] = 'neymar';
$_SESSION['prenom'] = 'jean';
$_SESSION['age'] = '51';

?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <title>session</title>
    </head>

    <body>

    <p>
    Salut <?php echo $_SESSION['prenom'].' '.$_SESSION['nom'].', tu as '.$_SESSION['age'].' ans.'; ?></br>
    </p>
    <a href="fin.php">partir</a>


    </body>

</html>